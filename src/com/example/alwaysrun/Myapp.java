package com.example.alwaysrun;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class Myapp extends Application {

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		IntentFilter filter = new IntentFilter(Intent.ACTION_TIME_TICK);
		MyBroadcastReceiver receiver = new MyBroadcastReceiver();
		registerReceiver(receiver, filter);
	}

	class MyBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			boolean isServiceRunning = false;
			ActivityManager manager = (ActivityManager) Myapp.this
					.getSystemService(Context.ACTIVITY_SERVICE);
			for (RunningServiceInfo service : manager
					.getRunningServices(Integer.MAX_VALUE)) {
				if ("com.example.alwaysrun.RunService".equals(service.service
						.getClassName()))
				// Service的类名
				{
					isServiceRunning = true;
				}

			}
			if (!isServiceRunning) {
				Intent i = new Intent(context, RunService.class);
				context.startService(i);
			}
		}
	}
}
